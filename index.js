// console.log("Hello World!");

// ARITHMETIC OPERATORS

let x = 1397;
let y = 7831;

// let sum = 1397 + 7831
let sum = x + y;

console.log("Result of addition operator: "+ sum);

let difference = x - y;
console.log("Result of subtraction operator: "+ difference);

let product = x * y;
console.log("Result of multiplication operator: "+ product);

let qoutient = x / y;
console.log("Result of division operator: "+ qoutient);

// modulus (%)
// gets the remainder from 2 divided values

let remainder = y % x;
console.log("Result of division modulo: "+ remainder);

// ASSIGNMENT OPERATOR (=)

// Basic Assignment 
let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// shorthand method for assignment operator (+=)
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber -= 2;
console.log("Result of substraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and parenthesis

let mdas= 1+2 -3 * 4 /5

console.log('Result of mdas oepratation: ' + mdas );

let pemdas = 1 + (2-3) * (4/5);
console.log('Result of pemdas oepratation: ' + pemdas );

pemdas = (1+ (2-3)) * (4/5);
console.log('Result of pemdas oepratation: ' + pemdas );

// Incrementation vs Decrementation
// Incrementation (++)
// Decrementation (--)
let z = 1;

// ++z added 1 value to its original value
let increment = ++z;
console.log('Result of pre-incrementation oepratation: ' + increment);
console.log('Result of pre-incrementation oepratation: ' + z );

increment = z++;
console.log('Result of post-incrementation oepratation: ' + increment );
console.log('Result of post-incrementation oepratation: ' + z );

let decrement = --z;
console.log('Result of pre-decrementation oepratation: ' + decrement );
console.log('Result of pre-decrementation  oepratation: ' + z );

decrement = z--;
console.log('Result of post-decrementation oepratation: ' + decrement );
console.log('Result of post-decrementation oepratation: ' + z );

// Type Coercion
let numA = "10";
let numB = 12;

let coercion = numA + numB;

console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC +numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// false = 0;
let numE = false + 1;

console.log(numE);

// true = 1;
let numF = true + 1;
console.log(numF);

// Comparison Operators

let juan ="juan";

// Equality Operator (==)
// Checks 2 operands if they are equal/have the same content
// May return boolean value

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == "1");
console.log("juan" == "juan");
console.log("juan" == juan);

// Inequality Operator (!=)
// ! == not
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log("juan" != "juan");
console.log("juan" != juan);
console.log(0 != false);

// Strict Equality Operator (===)
// Compares the content and also the data type

console.log(1 === 1);
console.log(1 === 2);
console.log("juan" == juan);
console.log(1 === "1");

// Strict Inequality Operator (===)
// Compares the content and also the data type

console.log(1 !== 1);
console.log(1 !== 2);
console.log("juan" !== juan);

// Relational Operator
let a = 50;
let b = 65;

// GT (>) Greater than operator
let isGreaterThan = a > b;
console.log(isGreaterThan);
// LT (<) Less than operator
let isLessThan = a < b;
console.log(isLessThan);
// GTE (>) Greater than Equal operator
let isLTorEqual = a >= b;
console.log(isLTorEqual);
// LTE (<) Less than Equal operator

let numStr = "30";
console.log(a > numStr);

let str ="twenty";
// console (b >= str);


//In some events, we can receive NaN
// NaN == Not a Number

//Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& - Ampersands)
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical AND Operator: " + allRequirementsMet);

// Logical OR Operator (|| - Double Pipe)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Logical OR Operator: " + someRequirementsMet);

// Logical NOT Operator (! - Exclamation Point)
//Returns Opposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result of Logical NOT Operator: " + someRequirementsNotMet);
